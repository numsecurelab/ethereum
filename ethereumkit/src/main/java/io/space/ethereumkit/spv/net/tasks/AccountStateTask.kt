package io.space.ethereumkit.spv.net.tasks

import io.space.ethereumkit.spv.core.ITask
import io.space.ethereumkit.spv.models.BlockHeader

class AccountStateTask(val address: ByteArray, val blockHeader: BlockHeader): ITask
