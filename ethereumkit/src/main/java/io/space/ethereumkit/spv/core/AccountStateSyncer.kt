package io.space.ethereumkit.spv.core

import io.space.ethereumkit.core.ISpvStorage
import io.space.ethereumkit.spv.models.AccountState
import io.space.ethereumkit.spv.models.BlockHeader
import io.space.ethereumkit.spv.net.handlers.AccountStateTaskHandler
import io.space.ethereumkit.spv.net.tasks.AccountStateTask

class AccountStateSyncer(private val storage: ISpvStorage,
                         private val address: ByteArray) : AccountStateTaskHandler.Listener {

    interface Listener {
        fun onUpdate(accountState: AccountState)
    }

    var listener: Listener? = null

    fun sync(taskPerformer: ITaskPerformer, blockHeader: BlockHeader) {
        taskPerformer.add(AccountStateTask(address, blockHeader))
    }

    override fun didReceive(accountState: AccountState, address: ByteArray, blockHeader: BlockHeader) {
        storage.saveAccountSate(accountState)
        listener?.onUpdate(accountState)
    }

}
