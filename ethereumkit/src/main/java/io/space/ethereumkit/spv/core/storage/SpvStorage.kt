package io.space.ethereumkit.spv.core.storage

import io.space.ethereumkit.core.ISpvStorage
import io.space.ethereumkit.spv.models.AccountState
import io.space.ethereumkit.spv.models.BlockHeader

class SpvStorage(private val database: SpvDatabase) : ISpvStorage {

    override fun getLastBlockHeader(): BlockHeader? {
        return database.blockHeaderDao().getAll().firstOrNull()
    }

    override fun saveBlockHeaders(blockHeaders: List<BlockHeader>) {
        return database.blockHeaderDao().insertAll(blockHeaders)
    }

    override fun getBlockHeadersReversed(fromBlockHeight: Long, limit: Int): List<BlockHeader> {
        return database.blockHeaderDao().getByBlockHeightRange(fromBlockHeight - limit, fromBlockHeight)
    }

    override fun getAccountState(): AccountState? {
        return database.accountStateDao().getAccountState()
    }

    override fun saveAccountSate(accountState: AccountState) {
        database.accountStateDao().insert(accountState)
    }

}
