package io.space.ethereumkit.spv.net.tasks

import io.space.ethereumkit.spv.core.ITask
import io.space.ethereumkit.spv.models.RawTransaction
import io.space.ethereumkit.spv.models.Signature

class SendTransactionTask(val sendId: Int,
                          val rawTransaction: RawTransaction,
                          val nonce: Long,
                          val signature: Signature) : ITask
